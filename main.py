import os

import cv2
import numpy as np
from matplotlib import pyplot as plt
from skimage.measure import label
from numba import jit
from scipy.ndimage import rotate
import scipy
import sys
from skimage.morphology import binary_closing, binary_dilation, binary_erosion, binary_opening

np.set_printoptions(threshold=sys.maxsize)

penciles = 0

type1 = np.array([[0, 1, 0],
                  [0, 1, 0],
                  [0, 1, 0],
                  ])


@jit
def averaging_same_size(image, x_blocks, y_blocks):
    new_img = np.zeros((x_blocks, y_blocks))
    image = image.copy()
    x_size = image.shape[0] // x_blocks
    y_size = image.shape[1] // y_blocks

    for i in range(x_blocks):
        for j in range(y_blocks):
            if i == x_blocks - 1 and j == y_blocks:
                new_img[i:, j:] = \
                    np.mean(image[i * x_size:, j * y_size:])
            elif i == x_blocks:
                new_img[i:, j: (j + 1)] = \
                    np.mean(image[i * x_size:, j * y_size: (j + 1) * y_size])
            elif j == y_blocks:
                new_img[i: (i + 1), j:] = \
                    np.mean(image[i * x_size: (i + 1) * x_size, j * y_size:])
            else:
                new_img[i: (i + 1), j: (j + 1)] = \
                    np.mean(image[i * x_size: (i + 1) * x_size, j * y_size: (j + 1) * y_size])
    return new_img


def do_rotation(labeled, y_min, y_max, x_min, x_max, i):
    min_width = 10000000
    min_degree = 0
    for j in range(36):
        detail = labeled[y_min: y_max, x_min:x_max].copy()
        detail[detail != i] = 0
        detail = scipy.ndimage.rotate(detail, angle=5 * j)
        wh = np.where(detail == i)
        if len(wh[0]) < 1 or len(wh[0]) < 1:
            continue
        y_min1 = wh[0].min() - 1
        x_min1 = wh[1].min() - 1
        y_max1 = wh[0].max() + 2
        x_max1 = wh[1].max() + 2
        if min_width > x_max1 - x_min1:
            min_width = x_max1 - x_min1
            min_degree = 5 * j
    return min_degree


def prepare_img(image):
    im_gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
    ret2, im_gray_th_otsu = cv2.threshold(im_gray, 0, 255, cv2.THRESH_OTSU)
    im_gray_th_otsu = averaging_same_size(im_gray_th_otsu, 150, 150)
    return cv2.threshold(im_gray_th_otsu, 200, 255, cv2.THRESH_BINARY_INV)


for file in os.listdir('images'):
    penciles_on_photo = 0
    print(file, end=" ")
    image = cv2.imread("images/" + file)
    ret, im_gray_th_otsu = prepare_img(image)

    labeled = label(im_gray_th_otsu)
    # plt.title(file)
    # plt.imshow(labeled)
    # plt.show()
    for i in range(1, labeled.max() + 1):
        wh = np.where(labeled == i)
        area = len(wh[0])
        if area < 10:
            continue
        wh = np.where(labeled == i)
        y_min = wh[0].min() - 1
        x_min = wh[1].min() - 1
        y_max = wh[0].max() + 2
        x_max = wh[1].max() + 2

        min_degree = do_rotation(labeled, y_min, y_max, x_min, x_max, i)

        detail = labeled[y_min: y_max, x_min:x_max].copy()
        detail = rotate(detail, angle=min_degree)
        detail[detail != i] = 0
        detail[detail == i] = 1
        detail = binary_opening(detail, type1)
        wh = np.where(detail == 1)
        if len(wh[0]) < 1 or len(wh[0]) < 1:
            continue
        y_min1 = wh[0].min() - 1
        x_min1 = wh[1].min() - 1
        y_max1 = wh[0].max() + 2
        x_max1 = wh[1].max() + 2
        ratio = (y_max1 - y_min1) / (x_max1 - x_min1)
        # print(ratio)
        if ratio > 8 and x_max1 - x_min1 > 2:
            # plt.title(file)
            # plt.imshow(detail)
            # plt.show()
            penciles += 1
            penciles_on_photo += 1
    print(penciles_on_photo)
print(f"Total penciles: {penciles}")
